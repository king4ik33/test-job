<?php

namespace App\Http\Resources;

use App\Models\DeviceData;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin DeviceData */
class DeviceDataResource extends JsonResource
{
    public function toArray($request): array
        {
            return [
                'device' => $this->device,
                'data' => new SensorsResource($this->data),
                'datetime' => $this->datetime,
            ];
        }
}