<?php

namespace App\Http\Resources;

use App\Models\Sensor;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Sensor */
class SensorsResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'sensor' => $this->sensor,
            'temp' => $this->temp,
        ];
    }
}