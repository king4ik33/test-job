<?php

namespace App\Models;

use App\Models\Factories\DeviceDataFactory;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $device
 * @property Sensor[] $data
 * @property DateTime $datetime
 */
class DeviceData extends Model
{
    use HasFactory;

    const FILLABLE = ['device', 'data', 'datetime'];
    protected $fillable = self::FILLABLE;

    public static function factory(): DeviceDataFactory
    {
        return DeviceDataFactory::new();
    }
}