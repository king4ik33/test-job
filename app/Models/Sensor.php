<?php

namespace App\Models;

use App\Models\Factories\SensorFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $sensor
 * @property int $temp
 */
class Sensor extends Model
{
    use HasFactory;

    const FILLABLE = ['sensor', 'temp'];
    protected $fillable = self::FILLABLE;

    public static function factory(): SensorFactory
    {
        return SensorFactory::new();
    }
}