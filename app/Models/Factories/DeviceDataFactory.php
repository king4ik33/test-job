<?php

namespace App\Models\Factories;

use App\Models\DeviceData;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class DeviceDataFactory extends Factory
{
    protected $model = DeviceData::class;

    public function definition(): array
    {
        return [
            'datetime' => Carbon::now()->format('Y-m-d H:i:s'),
            'device' => $this->faker->numberBetween(1,100),
            'data' => SensorFactory::new()->count(rand(1,3))->make()->toArray(),
        ];
    }
}