<?php

namespace App\Models\Factories;

use App\Models\Sensor;
use Illuminate\Database\Eloquent\Factories\Factory;

class SensorFactory extends Factory
{
    protected $model = Sensor::class;

    public function definition(): array
    {
        return [
            'sensor' => $this->faker->numberBetween(1,3),
            'temp' => $this->faker->numberBetween(2,20),
        ];
    }
}