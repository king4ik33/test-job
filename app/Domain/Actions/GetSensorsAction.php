<?php

namespace App\Domain\Actions;

use Carbon\Carbon;
use ClickHouseDB\Client;

class GetSensorsAction
{
    private Client $db;

    public function __construct()
    {
        $this->db = new Client([
            'host' => config('services.clickhouse.host'),
            'port' => config('services.clickhouse.port'),
            'username' => config('services.clickhouse.username'),
            'password' => config('services.clickhouse.password'),
        ]);
    }

    public function execute($filter = [])
    {
        $filter = $this->setFilter($filter);

        $this->db->database('telemetry');
        $this->db->enableQueryConditions();

        $data = $this->db->select($this->getSelect($filter));

        return $data->rows();
    }

    private function getSelect($filter): string
    {
        $select = "SELECT * FROM metrics";
        $select .= " WHERE datetime >= {$filter['from']}";
        $select .= " AND datetime <= {$filter['to']}";
        if (!empty($filter['sensor'])) {
            $select .= " AND sensor = {$filter['sensor']}";
        }

        return $select;
    }

    private function setFilter($filter): array
    {
        $resultFilter = [];

        if (!empty($filter['from']) && !empty($filter['to'])) {
            $resultFilter['from'] = $filter['from'];
            $resultFilter['to'] = $filter['to'];
        } else {
            $resultFilter['from'] = Carbon::now()->startOfMonth();
            $resultFilter['to'] = Carbon::now()->endOfMonth();
        }
        if (!empty($filter['sensor'])) {
            $resultFilter['sensor'] = $filter['sensor'];
        }

        return $resultFilter;
    }
}