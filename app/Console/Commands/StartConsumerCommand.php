<?php

namespace App\Console\Commands;

use ClickHouseDB\Client;
use Exception;
use Illuminate\Console\Command;
use PhpAmqpLib\Channel\AbstractChannel;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class StartConsumerCommand extends Command
{
    public const EXCHANGE_NAME = 'work_exchange';
    public const QUEUE_NAME = 'test';
    public const CLICKHOUSE_DB = 'telemetry';
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'work:consumer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Запуск consumer-а';

    /**
     * Execute the console command.
     * @throws Exception
     */
    public function handle()
    {
        $channel = $this->prepareRabbitChannel();
        $db = $this->getDBTelemetry();

        $channel->basic_consume('test', callback: function ($message) use ($db) {
            $messageData = json_decode($message->body);
            $datetime = $messageData->datetime;
            $device = $messageData->device;
            foreach ($messageData->data as $item) {
                echo "Insert data \n";
                $db->insert('metrics',
                    [
                        [$datetime, $device, $item->sensor, $item->temp]
                    ],
                    ['datetime', 'device', 'sensor', 'temp']
                );
            }
        });

        while ($channel->is_open()) {
            $channel->wait();
        }
    }

    /** @throws Exception */
    private function getRabbitChannel(): AMQPChannel|AbstractChannel
    {
        $connection = new AMQPStreamConnection(
            config('services.rabbitmq.host'),
            config('services.rabbitmq.port'),
            config('services.rabbitmq.username'),
            config('services.rabbitmq.password')
        );

        return $connection->channel();
    }

    /** @throws Exception */
    private function prepareRabbitChannel(): AMQPChannel|AbstractChannel
    {
        $channel = $this->getRabbitChannel();

        $channel->exchange_declare(self::EXCHANGE_NAME, 'direct');
        $channel->queue_declare(self::QUEUE_NAME);
        $channel->queue_bind(self::QUEUE_NAME,self::EXCHANGE_NAME);

        return $channel;
    }

    private function getDBTelemetry(): Client
    {
        $db = new Client([
            'host' => config('services.clickhouse.host'),
            'port' => config('services.clickhouse.port'),
            'username' => config('services.clickhouse.username'),
            'password' => config('services.clickhouse.password'),
        ]);
        $db->database(self::CLICKHOUSE_DB);
        $db->ping(true);

        return $db;
    }
}
