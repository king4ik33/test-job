<?php

namespace App\Console\Commands;

use App\Models\Factories\DeviceDataFactory;
use Exception;
use Illuminate\Console\Command;
use PhpAmqpLib\Channel\AbstractChannel;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class GenerateEventsCommand extends Command
{
    public const QUEUE_NAME = 'test';
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'work:generate {count} {timeout?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генерация тестовых данных';

    /**
     * Execute the console command.
     * @throws Exception
     */
    public function handle()
    {
        $count = $this->argument('count');
        $timeout = $this->argument('timeout') ?? 20;

        if (!$count) {
            throw new Exception('Кол-во должно быть указано обязательно');
        }

        $i = 0;
        $timeoutMicroSec = $timeout * 1000;

        $channel = $this->getRabbitChannel();
        $channel->queue_declare(self::QUEUE_NAME);

        while ($i < $count) {
            $i++;
            echo "Generating data {$i} \n";
            $message = DeviceDataFactory::new()->makeOne()->toJson();
            $messageToRabbit = new AMQPMessage($message);
            $channel->basic_publish($messageToRabbit, routing_key: self::QUEUE_NAME);
            usleep($timeoutMicroSec);
        }

        $channel->close();
    }

    /** @throws Exception */
    private function getRabbitChannel(): AbstractChannel|AMQPChannel
    {
        $connection = new AMQPStreamConnection(
            config('services.rabbitmq.host'),
            config('services.rabbitmq.port'),
            config('services.rabbitmq.username'),
            config('services.rabbitmq.password')
        );

        return $connection->channel();
    }
}
